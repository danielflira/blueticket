package br.com.bandtec.catraca;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Locale;

import android.os.AsyncTask;

public class ReadTickets extends AsyncTask<String, Integer, String> {
	
	private ReadTicketCallBack callback;
	
	public void setCallBack(ReadTicketCallBack callback) {
		this.callback = callback;
	}
	
	@Override
	protected String doInBackground(String... params) {
		String retorno = null;
		
		if (params.length < 2 || params[0] == null || params[1] == null)
			return retorno;
		
		String urlBase = String.format(Locale.ENGLISH, 
				"http://%s/ticket/servidor/consultar/%s", params[0], params[1]);
		
		try {
			URL url = new URL(urlBase);
			
			int caracter;
			StringBuffer sb = new StringBuffer();
			
			InputStream is = url.openConnection().getInputStream();;
			while ((caracter = is.read()) != -1) {
				sb.append((char) caracter);
			}
			
			retorno = sb.toString();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return retorno;
	}

	@Override
	protected void onPostExecute(String result) {
		if ( callback != null )
			callback.onFinish(result);
	}

}
