package br.com.bandtec.catraca;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.app.Activity;
import android.graphics.Color;

public class MainActivity extends Activity
	implements OnClickListener, ReadTicketCallBack {
	
	private String tickets;
	private EditText txtServer;
	private EditText txtNumero;
	private TextView output;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		readFromFile();
		
		findViewById(R.id.button1).setOnClickListener(this);
		txtServer = (EditText) findViewById(R.id.editText1);
		txtNumero = (EditText) findViewById(R.id.editText2);
		output = (TextView) findViewById(R.id.textView3);
		
		processTickets();
		output.setText(tickets);
	}
	
	private void processTickets() {
		if (NfcAdapter.ACTION_NDEF_DISCOVERED.equals(getIntent().getAction())) {
			Parcelable[] rawMsgs = getIntent().getParcelableArrayExtra(
					NfcAdapter.EXTRA_NDEF_MESSAGES);
			
			if ( rawMsgs != null ) {
				String msg = "";
				boolean removido = false;
				for ( Parcelable tmp1 : rawMsgs ) {
					NdefMessage ndefMessage = (NdefMessage) tmp1;
					for ( NdefRecord tmp2 : ndefMessage.getRecords() ) {
						msg += new String(tmp2.getPayload());
					}
				}
				
				for ( String tmp : msg.split("@") ) {
					if ( tmp != "" && tickets.contains(tmp) ) {
						tickets = tickets.replace(tmp + "@", "");
						Toast.makeText(this, "Liberado", Toast.LENGTH_LONG).show();
						saveOnFile();
						removido = true;
					}
				}
				
				if ( !removido )
					Toast.makeText(this, "Bloqueado", Toast.LENGTH_LONG).show();
				
				finish();
			}
		}
	}

	@Override
	public void onClick(View v) {
		switch ( v.getId() ) {
		case R.id.button1:
			getTickets();
			break;
		}
	}
	
	private void getTickets() {
		ReadTickets readTickets = new ReadTickets();
		readTickets.setCallBack(this);
		
		readTickets.execute(txtServer.getText().toString(), 
				txtNumero.getText().toString());
	}

	@Override
	public void onFinish(String tickets) {
		this.tickets = tickets;
		output.setText(tickets);
		saveOnFile();
	}
	
	@Override
	protected void onStop() {
		super.onStop();
		saveOnFile();
	}
	
	private void saveOnFile() {
		if ( tickets == null )
			return;
		
		FileOutputStream fos;
		try {
			fos = openFileOutput("tickets.txt", MODE_PRIVATE);
			fos.write(tickets.getBytes());
			fos.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void readFromFile() {
		FileInputStream fis;
		
		int caracter;
		StringBuffer sb = new StringBuffer();
		
		try {
			fis = openFileInput("tickets.txt");
			
			while ( (caracter = fis.read()) != -1 ) {
				sb.append((char) caracter);
			}
			
			this.tickets = sb.toString();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
