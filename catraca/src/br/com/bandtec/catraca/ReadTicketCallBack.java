package br.com.bandtec.catraca;

public interface ReadTicketCallBack {
	public abstract void onFinish(String tickets);
}
