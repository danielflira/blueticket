package br.com.bandtec.blueticket.gcm;

import java.io.IOException;

import com.google.android.gms.gcm.GoogleCloudMessaging;

import android.content.Context;
import android.os.AsyncTask;

public class GcmTask extends AsyncTask<String, Integer, String> {
	
	private static final String SENDERID = "259837542304";
	public static final int UNREGISTER = 2;
	public static final int REGISTER = 1;
	public static final String UNREGISTER_SUCCESSFUL = "Ok";
	
	private Context context;
	private GoogleCloudMessaging gcmInstance;
	private GcmActionCallback callback;
	private int rotine;
	private int lastRotine;
	
	public GcmTask(Context context, GcmActionCallback callback) {
		this.context = context;
		this.callback = callback;
		this.rotine = 0;
		gcmInstance = GoogleCloudMessaging.getInstance(this.context);
	}
	
	public void setRotine(int rotine) {
		this.rotine = rotine;
	}
	
	private String register() {
		try {
			return gcmInstance.register(SENDERID);
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	private String unregister() {
		try {
			gcmInstance.unregister();
			return UNREGISTER_SUCCESSFUL;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	@Override
	protected String doInBackground(String... params) {
		switch(rotine) {
		case REGISTER:
			lastRotine = REGISTER;
			return register();
		case UNREGISTER:
			lastRotine = UNREGISTER;
			unregister();
		}
		return null;
	}

	@Override
	protected void onPostExecute(String result) {
		super.onPostExecute(result);
		
		switch(lastRotine) {
		case REGISTER:
			callback.onGcmRegister(result);
			break;
		case UNREGISTER:
			if ( result != null && result == UNREGISTER_SUCCESSFUL )
				callback.onGcmUnregister(true);
			callback.onGcmUnregister(false);
			break;
		}
	}
}
