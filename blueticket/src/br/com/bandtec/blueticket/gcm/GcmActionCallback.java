package br.com.bandtec.blueticket.gcm;

public interface GcmActionCallback {
	public void onGcmRegister(String gcmid);
	public void onGcmUnregister(boolean result);
}
