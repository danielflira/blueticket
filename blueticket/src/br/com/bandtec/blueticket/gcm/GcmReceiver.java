package br.com.bandtec.blueticket.gcm;

import br.com.bandtec.blueticket.storage.android.service.StorageTicketService;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

public class GcmReceiver extends BroadcastReceiver {
	
	@Override
	public void onReceive(Context context, Intent intent) {
		Bundle extra = intent.getExtras();
		StorageTicketService storage = StorageTicketService.getInstance();
		
		if ( storage != null )
			storage.save(extra.getString("message"), storage);
		
		setResultCode(Activity.RESULT_OK);
	}
}
