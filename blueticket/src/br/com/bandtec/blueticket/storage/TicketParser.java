package br.com.bandtec.blueticket.storage;

import java.util.List;

public interface TicketParser
{
	public abstract Ticket parse(String ticket) throws BadTicket;
	public abstract String format(List<Ticket> ticket) throws BadTicket;
	public abstract String genMD5(String ticket) throws BadTicket;
}