package br.com.bandtec.blueticket.storage.android.service;

import java.util.ArrayList;
import java.util.Date;

import br.com.bandtec.blueticket.storage.BadTicket;
import br.com.bandtec.blueticket.storage.Ticket;
import br.com.bandtec.blueticket.storage.TicketParser;
import br.com.bandtec.blueticket.storage.TicketParserImpl;
import br.com.bandtec.blueticket.storage.android.TicketDAO;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.widget.Toast;

public class StorageTicketService extends Service 
	implements StorageServiceCallback {
	
	private static StorageTicketService self = null;
	private TicketDAO ticketDAO = null;
	private TicketParser ticketParser = null;
	
	@Override
	public IBinder onBind(Intent arg0) {
		return null;
	}
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		super.onStartCommand(intent, flags, startId);
		
		if ( self == null )
			self = this;
		
		ticketDAO = new TicketDAO(this);
		ticketParser = new TicketParserImpl();
		
		return startId;
	}
	
	public static StorageTicketService getInstance() {
		if ( self == null )
			return null;
		return self;
	}
	
	public void save(String ticket, StorageServiceCallback callback) {
		try {
			boolean resultado = ticketDAO.save(ticketParser.parse(ticket));
			callback.onSave(resultado);
		} catch (BadTicket e) {
			e.printStackTrace();
			callback.onSave(false);
		}
	}
	
	public void search(Date date, StorageServiceCallback callback) {
		callback.onSearch(ticketDAO.search(date));
	}
	
	public void expire(StorageServiceCallback callback) {
		callback.onExpire(ticketDAO.expire());
	}
	
	public void delete(String ticket, StorageServiceCallback callback) {
		try {
			boolean resultado = ticketDAO.delete(ticketParser.parse(ticket));
			callback.onDelete(resultado);
		} catch (BadTicket e) {
			e.printStackTrace();
			callback.onDelete(false);
		}
	}
	
	public void list(StorageServiceCallback callback) {
		callback.onList(ticketDAO.list());
	}

	@Override
	public void onSave(boolean result) {
		if ( result )
			Toast.makeText(this, "Ticket recebido com sucesso", Toast.LENGTH_LONG).show();
		else
			Toast.makeText(this, "Erro ao receber ticket", Toast.LENGTH_LONG).show();
	}

	@Override
	public void onSearch(ArrayList<Ticket> tickets) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onExpire(boolean result) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onDelete(boolean result) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onList(ArrayList<Ticket> tickets) {
		// TODO Auto-generated method stub
		
	}
}
