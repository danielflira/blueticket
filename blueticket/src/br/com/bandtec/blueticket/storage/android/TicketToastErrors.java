package br.com.bandtec.blueticket.storage.android;

import android.content.Context;
import android.widget.Toast;
import br.com.bandtec.blueticket.storage.ErrorTicket;

public class TicketToastErrors {
	public static void toastErrors(Context app) {
		ErrorTicket errors = ErrorTicket.getInstance();
		for (String error: errors.getAllMessages())
			Toast.makeText(app, error, Toast.LENGTH_LONG).show();
	}
}
