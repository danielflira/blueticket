package br.com.bandtec.blueticket.storage.android.service;

import java.util.ArrayList;

import br.com.bandtec.blueticket.storage.Ticket;

public interface StorageServiceCallback {
	public void onSave(boolean result);
	public void onSearch(ArrayList<Ticket> tickets);
	public void onExpire(boolean result);
	public void onDelete(boolean result);
	public void onList(ArrayList<Ticket> tickets);
}
