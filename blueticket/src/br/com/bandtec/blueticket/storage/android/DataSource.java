package br.com.bandtec.blueticket.storage.android;

import br.com.bandtec.blueticket.R;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DataSource extends SQLiteOpenHelper
{
	private Context context;
	
	public DataSource(Context context) {
		super(context, "ticket.db", null, 1);
		this.context = context;
	}
	
	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(this.context.getString(
				R.string.create_eticket));
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		
	}
	
	public Context getContext() {
		return context;
	}
}
