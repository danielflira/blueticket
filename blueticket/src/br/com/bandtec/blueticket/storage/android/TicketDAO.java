package br.com.bandtec.blueticket.storage.android;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import br.com.bandtec.blueticket.R;
import br.com.bandtec.blueticket.storage.Ticket;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class TicketDAO {
	DataSource datasource;
	SQLiteDatabase db;
	SimpleDateFormat sdf;

	public TicketDAO(Context context) {
		datasource = new DataSource(context);
		db = datasource.getWritableDatabase();
		sdf = new SimpleDateFormat("yyyyMMdd", Locale.ENGLISH);
	}

	private ArrayList<Ticket> getFromCursor(Cursor c) {
		ArrayList<Ticket> tickets = new ArrayList<Ticket>();

		if (c.moveToFirst()) {
			do {
				Ticket ticket = new Ticket();

				try {
					ticket.setRecebimento(sdf.parse(c.getString(0)));
				} catch (ParseException e) {
					e.printStackTrace();
					return tickets;
				}

				if (c.getString(1) != null) {
					try {
						ticket.setVencimento(sdf.parse(c.getString(1)));
					} catch (ParseException e) {
						e.printStackTrace();
						return tickets;
					}
				}

				ticket.setNumero(c.getInt(2));
				ticket.setNome(c.getString(3));
				ticket.setCpf(c.getString(4));
				ticket.setToken(c.getString(5));

				tickets.add(ticket);
			} while (c.moveToNext());
		}

		return tickets;
	}

	public boolean save(Ticket ticket) {
		String[] data = new String[] { sdf.format(ticket.getRecebimento()),
				sdf.format(ticket.getVencimento()),
				Integer.toString(ticket.getNumero()), ticket.getNome(),
				ticket.getCpf(), ticket.getToken() };

		db.execSQL(datasource.getContext().getString(
				R.string.insert_eticket), data);

		return true;
	}

	public ArrayList<Ticket> search(Date date) {
		String[] data = new String[] { sdf.format(date) };

		Cursor c = db.rawQuery(datasource.getContext().getString(
				R.string.select_today), data);

		return getFromCursor(c);
	}

	public boolean expire() {
		String[] data = new String[] { sdf.format(new Date()) };
		
		db.execSQL(datasource.getContext().getString(
				R.string.delete_expired), data);
		
		return true;
	}

	public boolean delete(Ticket ticket) {
		String[] filter = new String[] { ticket.getToken() };
		
		db.execSQL(datasource.getContext().getString(
				R.string.delete_ticket), filter);
		
		return true;
	}

	public ArrayList<Ticket> list() {
		Cursor c = db.rawQuery(datasource.getContext().getString(
				R.string.select_all), null);

		return getFromCursor(c);
	}
}
