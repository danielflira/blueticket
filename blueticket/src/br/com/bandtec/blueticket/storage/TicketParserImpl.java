package br.com.bandtec.blueticket.storage;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

import br.com.bandtec.blueticket.storage.util.ByteArrayHex;

public class TicketParserImpl implements TicketParser
{
	private static SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd", Locale.ENGLISH);
	private ErrorTicket error;
	
	public TicketParserImpl() {
		error = ErrorTicket.getInstance();
	}
	
	@Override
	public Ticket parse(String ticket) throws BadTicket
	{
		String[] fields = ticket.split("\\|");
		Ticket t = new Ticket();
		
		try {
			t.setRecebimento(sdf.parse(fields[0]));
			t.setVencimento(sdf.parse(fields[1]));
		} catch (ParseException e) {
			String msg = "Data do ticket invalida";
			error.addMessage(msg);
			throw new BadTicket(msg);
		}
		
		t.setNumero(Integer.parseInt(fields[2]));
		t.setNome(fields[3]);
		t.setCpf(fields[4]);
		t.setToken(fields[5]);
		
		validate(t);
		return t;
	}
	
	public String genMD5(String ticket) throws BadTicket {
		// Se nao posso conferir o ticket � um BadTicket
		try {
			MessageDigest md5 = MessageDigest.getInstance("MD5");
			ByteArrayHex hex = new ByteArrayHex();
			byte[] md5arr = ticket.getBytes();
			
			return hex.getHexArray(md5.digest(md5arr));
		} catch (NoSuchAlgorithmException e) {
			String msg = "Ticket nao pode ser conferido";
			error.addMessage(msg);
			throw new BadTicket(msg);
		}
	}
	
	public boolean validate(Ticket ticket) throws BadTicket
	{
		StringBuilder t = new StringBuilder();
		
		t.append(sdf.format(ticket.getRecebimento()));
		t.append("|");
		t.append(sdf.format(ticket.getVencimento()));
		t.append("|");
		t.append(ticket.getNumero());
		t.append("|");
		t.append(ticket.getNome());
		t.append("|");
		t.append(ticket.getCpf());
		
		if(!genMD5(t.toString()).equals(ticket.getToken())) {
			String msg = "Ticket com o verificador invalido";
			error.addMessage(msg);
			throw new BadTicket(msg);
		}
		
		return true;
	}

	@Override
	public String format(List<Ticket> ticket) throws BadTicket
	{
		StringBuilder t = new StringBuilder();
		
		for(Ticket tmp: ticket) {
			t.append(sdf.format(tmp.getRecebimento()));
			t.append("|");
			t.append(sdf.format(tmp.getVencimento()));
			t.append("|");
			t.append(tmp.getNumero());
			t.append("|");
			t.append(tmp.getNome());
			t.append("|");
			t.append(tmp.getCpf());
			t.append("|");
			t.append(tmp.getToken());
			t.append("@");
		}
		
		return t.toString();
	}
}
