package br.com.bandtec.blueticket.storage;

public class BadTicket extends Exception
{
	private static final long serialVersionUID = -7834228355730387878L;
	
	public BadTicket(String message)
	{
		super(message);
	}
}
