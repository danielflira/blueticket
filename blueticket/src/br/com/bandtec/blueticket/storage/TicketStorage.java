package br.com.bandtec.blueticket.storage;

import java.util.List;

public interface TicketStorage
{
	public abstract boolean save(Ticket ticket);
	public abstract List<Ticket> check();
	public abstract boolean expire();
	public abstract boolean delete(Ticket ticket);
	public abstract List<Ticket> list();
}
