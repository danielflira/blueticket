package br.com.bandtec.blueticket.storage;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class Ticket
{
	private Date recebimento;
	private Date vencimento;
	private int numero;
	private String nome;
	private String cpf;
	private String token;
	
	private SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd", Locale.ENGLISH);
	
	public Date getRecebimento() {
		return recebimento;
	}

	public void setRecebimento(Date recebimento) {
		this.recebimento = recebimento;
	}

	public Date getVencimento() {
		return vencimento;
	}

	public void setVencimento(Date vencimento) {
		this.vencimento = vencimento;
	}

	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	@Override
	public String toString() {
		StringBuilder t = new StringBuilder();
		
		t.append(sdf.format(getRecebimento()));
		t.append("|");
		t.append(sdf.format(getVencimento()));
		t.append("|");
		t.append(getNumero());
		t.append("|");
		t.append(getNome());
		t.append("|");
		t.append(getCpf());
		t.append("|");
		t.append(getToken());
		
		return t.toString();
	}
}

