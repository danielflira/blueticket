package br.com.bandtec.blueticket.storage.util;

public class ByteArrayHex
{
	public String getHexArray(byte[] bytes)
	{
		StringBuilder s = new StringBuilder();
		
		for (int i=0; i < bytes.length; i++) {
			s.append(getHex(getByteHigh(bytes[i])));
			s.append(getHex(getByteLow(bytes[i])));
		}
		
		return s.toString();
	}
	
	public char getHex(byte b)
	{
		char[] c = {
			'0', '1', '2', '3', '4', '5', '6', '7',
			'8', '9', 'a', 'b', 'c', 'd', 'e', 'f'
		};
		return c[b];
	}
	
	public byte getByteLow(byte b)
	{
		return (byte) (b & 0x0F);
	}
	
	public byte getByteHigh(byte b)
	{
		return (byte) ((b & 0xF0) >> 4);
	}

}
