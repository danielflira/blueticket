package br.com.bandtec.blueticket.storage;

import java.util.ArrayList;

public class ErrorTicket {
	private static ErrorTicket errorTicket = null;
	private ArrayList<String> messages = null;
	
	private ErrorTicket() {
		messages = new ArrayList<String>();
	}
	
	public static ErrorTicket getInstance() {
		if ( errorTicket == null )
			errorTicket = new ErrorTicket();
		return errorTicket;
	}
	
	public void addMessage(String message) {
		messages.add(message);
	}
	
	public ArrayList<String> getAllMessages() {
		ArrayList<String> tmp = messages;
		messages = new ArrayList<String>();
		return tmp;
	}
}
