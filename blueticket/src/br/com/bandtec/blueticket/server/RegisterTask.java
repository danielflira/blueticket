package br.com.bandtec.blueticket.server;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import android.os.AsyncTask;

public class RegisterTask extends AsyncTask<String, Integer, String>{
	
private static final String SERVER_URL = "http://%s/ticket/celular/registrar/%s";
	
	private MobileRegisterCallback callback;
	private String cpf;
	private String gcmid;
	private String server;
	
	public RegisterTask(MobileRegisterCallback callback) {
		this.callback = callback;
	}
	
	public void setCPF(String cpf) {
		this.cpf = cpf;
	}
	
	public void setGCM(String gcmid) {
		this.gcmid = gcmid;
	}
	
	public void setServer(String server) {
		this.server = server;
	}
	
	private String register() {
		StringBuilder result = new StringBuilder();
		
		try {
			String parameter = "gcmid=" + gcmid;
			
			URL request = new URL(String.format(SERVER_URL, server, cpf));
			HttpURLConnection http = (HttpURLConnection) request.openConnection();
			http.setRequestMethod("POST");
			http.setRequestProperty("Content-Length", Integer.toString(parameter.length()));
			
			OutputStream os = http.getOutputStream();
			os.write(parameter.getBytes());
			os.flush();
			os.close();
			
			http.connect();
			
			int character;
			InputStream is = http.getInputStream();
			
			while((character = is.read()) != -1)
				result.append((char) character);
			
		} catch (MalformedURLException e) {
			e.printStackTrace();
			return null;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
		
		if (result.toString().equals("Ok"))
			return "Ok";
		
		return null;
	}

	@Override
	protected String doInBackground(String... params) {
		return register();
	}

	@Override
	protected void onPostExecute(String result) {
		super.onPostExecute(result);
		if ( result != null && result == "Ok" )
			callback.onServerRegister(true);
		callback.onServerRegister(false);
	}
}
