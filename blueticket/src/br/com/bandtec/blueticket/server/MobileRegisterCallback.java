package br.com.bandtec.blueticket.server;

public interface MobileRegisterCallback {
	public void onServerRegister(boolean result);
}
