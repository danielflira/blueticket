package br.com.bandtec.blueticket.android.ui;


import java.util.ArrayList;
import java.util.Date;

import br.com.bandtec.blueticket.R;
import br.com.bandtec.blueticket.storage.Ticket;
import br.com.bandtec.blueticket.storage.android.TicketToastErrors;
import br.com.bandtec.blueticket.storage.android.service.StorageServiceCallback;
import br.com.bandtec.blueticket.storage.android.service.StorageTicketService;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;

public class DeleteTicketActivity extends Activity
	implements OnClickListener, StorageServiceCallback, OnItemClickListener,
	DialogInterface.OnClickListener {
	
	private ListView list;
	private ArrayList<Ticket> tickets;
	StorageTicketService storage = StorageTicketService.getInstance();
	private int position;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_delete_ticket);
		
		findViewById(R.id.btnDeletarCancelar).setOnClickListener(this);
		list = (ListView) findViewById(R.id.listDeletarTicket);
		
		storage = StorageTicketService.getInstance();
		if ( storage != null )
			storage.search(new Date(), this);
	}
	
	@Override
	public void onClick(View v) {
		switch ( v.getId() ) {
			case R.id.btnDeletarCancelar:
				finish();
				break;
		}
	}

	@Override
	public void onSave(boolean result) {
	}

	@Override
	public void onSearch(ArrayList<Ticket> tickets) {
		if ( tickets != null ) {
			this.tickets = tickets;  
			
			ArrayList<String> tokens = new ArrayList<String>();
			for (Ticket ticket: tickets)
				tokens.add(ticket.toString());
			
			ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
					android.R.layout.simple_list_item_1,
					android.R.id.text1, tokens);
			
			list.setAdapter(adapter);
			list.setOnItemClickListener(this);
		} else {
			TicketToastErrors.toastErrors(this);
		}
	}

	@Override
	public void onExpire(boolean result) {
	}

	@Override
	public void onDelete(boolean result) {
	}

	@Override
	public void onList(ArrayList<Ticket> tickets) {
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		// FIXME: Remover texto fixo
		this.position = position;
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage("Deseja mesmo remover?").setPositiveButton("Sim", this)
		    .setNegativeButton("Nao", this).show();
		
	}

	@Override
	public void onClick(DialogInterface dialog, int which) {
		// FIXME: Remover texto fixo
		switch (which){
	        case DialogInterface.BUTTON_POSITIVE:
	        	storage.delete(tickets.get(position).toString(), this);
	        	Toast.makeText(this, "Removido com sucesso", Toast.LENGTH_LONG).show();
	        	finish();
	            break;
	        case DialogInterface.BUTTON_NEGATIVE:
	            break;
        }
	}
}
