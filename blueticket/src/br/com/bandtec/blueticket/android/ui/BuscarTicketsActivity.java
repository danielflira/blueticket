package br.com.bandtec.blueticket.android.ui;


import java.util.ArrayList;
import java.util.Date;

import br.com.bandtec.blueticket.R;
import br.com.bandtec.blueticket.storage.Ticket;
import br.com.bandtec.blueticket.storage.android.TicketToastErrors;
import br.com.bandtec.blueticket.storage.android.service.StorageServiceCallback;
import br.com.bandtec.blueticket.storage.android.service.StorageTicketService;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.app.Activity;

public class BuscarTicketsActivity extends Activity
	implements OnClickListener, StorageServiceCallback {
	private ListView list;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_buscar_tickets);
		
		findViewById(R.id.btnBuscarCancelar).setOnClickListener(this);
		list = (ListView) findViewById(R.id.listBuscarTickets);
		
		StorageTicketService storage = StorageTicketService.getInstance();
		if ( storage != null )
			storage.search(new Date(), this);
	}
	
	@Override
	public void onClick(View v) {
		switch ( v.getId() ) {
			case R.id.btnBuscarCancelar:
				finish();
				break;
		}
	}

	@Override
	public void onSave(boolean result) {
	}

	@Override
	public void onSearch(ArrayList<Ticket> tickets) {
		if ( tickets != null ) {
			ArrayList<String> tokens = new ArrayList<String>();
			for (Ticket ticket: tickets)
				tokens.add(ticket.toString());
			
			ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
					android.R.layout.simple_list_item_1,
					android.R.id.text1, tokens);
			
			list.setAdapter(adapter);
		} else {
			TicketToastErrors.toastErrors(this);
		}
	}

	@Override
	public void onExpire(boolean result) {
	}

	@Override
	public void onDelete(boolean result) {
	}

	@Override
	public void onList(ArrayList<Ticket> tickets) {
	}
}
