package br.com.bandtec.blueticket.android.ui;

import java.util.ArrayList;

import br.com.bandtec.blueticket.R;
import br.com.bandtec.blueticket.storage.BadTicket;
import br.com.bandtec.blueticket.storage.Ticket;
import br.com.bandtec.blueticket.storage.TicketParser;
import br.com.bandtec.blueticket.storage.TicketParserImpl;
import br.com.bandtec.blueticket.storage.android.TicketToastErrors;
import br.com.bandtec.blueticket.storage.android.service.StorageServiceCallback;
import br.com.bandtec.blueticket.storage.android.service.StorageTicketService;
import android.os.Bundle;
import android.app.Activity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class AdicionarTicketActivity extends Activity
	implements OnClickListener, StorageServiceCallback {
	
	private EditText recebimento;
	private EditText vencimento;
	private EditText numero;
	private EditText nome;
	private EditText cpf;
	private TextView token;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_adicionar_ticket);
		
		findViewById(R.id.btnAdicionarSalvar).setOnClickListener(this);
		findViewById(R.id.btnAdicionarCancelar).setOnClickListener(this);
		
		recebimento = (EditText) findViewById(R.id.editRecebimento);
		vencimento = (EditText) findViewById(R.id.editVencimento);
		numero = (EditText) findViewById(R.id.editNumero);
		nome = (EditText) findViewById(R.id.editNome);
		cpf = (EditText) findViewById(R.id.editCPF);
		token = (TextView) findViewById(R.id.editToken);
	}
	
	protected String validateTicket() {
		StringBuilder builder = new StringBuilder();
		builder.append(recebimento.getText());
		builder.append("|");
		builder.append(vencimento.getText());
		builder.append("|");
		builder.append(numero.getText());
		builder.append("|");
		builder.append(nome.getText());
		builder.append("|");
		builder.append(cpf.getText());
		
		TicketParser parser = new TicketParserImpl();
		try {
			String md5 = parser.genMD5(builder.toString());
			builder.append("|");
			builder.append(md5);
			token.setText(md5);
		} catch (BadTicket e) {
			TicketToastErrors.toastErrors(this);
			e.printStackTrace();
		}
		
		return builder.toString();
	}

	@Override
	public void onClick(View v) {
		StorageTicketService storage = StorageTicketService.getInstance();
		
		switch(v.getId()) {
			case R.id.btnAdicionarSalvar:
				if ( storage != null )
					storage.save(validateTicket(), this);
				break;
			case R.id.btnAdicionarCancelar:
				finish();
				break;
		}
	}

	@Override
	public void onSave(boolean result) {
		if ( result == false ) {
			TicketToastErrors.toastErrors(this);
		} else {
			Toast.makeText(this, "Salvo com sucesso", Toast.LENGTH_LONG).show();
			finish();
		}
	}

	@Override
	public void onSearch(ArrayList<Ticket> tickets) {
	}

	@Override
	public void onExpire(boolean result) {
	}

	@Override
	public void onDelete(boolean result) {
	}

	@Override
	public void onList(ArrayList<Ticket> tickets) {
	}
}
