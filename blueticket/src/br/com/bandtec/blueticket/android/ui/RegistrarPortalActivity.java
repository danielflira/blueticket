package br.com.bandtec.blueticket.android.ui;

import br.com.bandtec.blueticket.R;
import br.com.bandtec.blueticket.gcm.GcmActionCallback;
import br.com.bandtec.blueticket.gcm.GcmTask;
import br.com.bandtec.blueticket.server.MobileRegisterCallback;
import br.com.bandtec.blueticket.server.RegisterTask;


import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.app.Activity;

public class RegistrarPortalActivity extends Activity
	implements OnClickListener, GcmActionCallback, MobileRegisterCallback {
	
	private EditText cpf;
	private EditText server;
	private TextView gcm;
	
	private GcmTask gcmAction;
	private RegisterTask mobileRegister;
	private Button btnRegister;
	private Button btnUnregister;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_registrar_portal);
		
		cpf = (EditText) findViewById(R.id.editRegistrarCpf);
		server = (EditText) findViewById(R.id.editRegistrarServer);
		gcm = (TextView) findViewById(R.id.textRegistrarGcmid);
		btnRegister = (Button) findViewById(R.id.btnRegistrarGCM);
		btnUnregister = (Button) findViewById(R.id.btnDesregistrarGCM);
		
		btnRegister.setOnClickListener(this);
		btnUnregister.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch(v.getId()) {
			case R.id.btnRegistrarGCM:
				gcmAction = new GcmTask(this, this);
				mobileRegister = new RegisterTask(this);
				gcmAction.setRotine(GcmTask.REGISTER);
				gcmAction.execute(new String[] {});
				v.setEnabled(false);
				break;
			case R.id.btnDesregistrarGCM:
				gcmAction = new GcmTask(this, this);
				gcmAction.setRotine(GcmTask.UNREGISTER);
				gcmAction.execute(new String[] {});
				v.setEnabled(false);
				break;
		}
	}

	@Override
	public void onGcmRegister(String gcmid) {
		if (gcmid != null) {
			gcm.setText(gcmid);
			mobileRegister.setGCM(gcmid);
			mobileRegister.setCPF(cpf.getText().toString());
			mobileRegister.setServer(server.getText().toString());
			mobileRegister.execute(new String[] {});
		} else {
			gcm.setText("Nao foi possivel registrar");
		}
		btnRegister.setEnabled(true);
	}

	@Override
	public void onGcmUnregister(boolean result) {
		if ( result ) {
			gcm.setText("Removido registro com sucesso");
		} else {
			gcm.setText("N�o foi possivel remover registro");
		}
		btnUnregister.setEnabled(true);
	}

	@Override
	public void onServerRegister(boolean result) {
		if ( result ) {
			Toast.makeText(this, "Registrado no servidor", Toast.LENGTH_LONG).show();
		} else {
			Toast.makeText(this, "Nao registrado no servidor", Toast.LENGTH_LONG).show();
		}
	}
}
