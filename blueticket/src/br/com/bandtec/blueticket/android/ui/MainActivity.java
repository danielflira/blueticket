package br.com.bandtec.blueticket.android.ui;

import java.util.ArrayList;
import java.util.Date;

import br.com.bandtec.blueticket.R;
import br.com.bandtec.blueticket.storage.Ticket;
import br.com.bandtec.blueticket.storage.android.TicketToastErrors;
import br.com.bandtec.blueticket.storage.android.service.StorageServiceCallback;
import br.com.bandtec.blueticket.storage.android.service.StorageTicketService;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.os.Bundle;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Toast;

@SuppressLint("NewApi")
public class MainActivity extends Activity
	implements OnClickListener, StorageServiceCallback {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        starStorageService();
        
        findViewById(R.id.btnMainAdicionarTicket).setOnClickListener(this);
        findViewById(R.id.btnMainListarTickets).setOnClickListener(this);
        findViewById(R.id.btnMainBuscarTickets).setOnClickListener(this);
        findViewById(R.id.btnMainExpirarTickets).setOnClickListener(this);
        findViewById(R.id.btnMainDeletarTicket).setOnClickListener(this);
        findViewById(R.id.btnMainRegistrarCelular).setOnClickListener(this);
        findViewById(R.id.btnMainEnviarTickets).setOnClickListener(this);
    }
    
    private void starStorageService() {
    	startService(new Intent(this, StorageTicketService.class));
    }

	@Override
	public void onClick(View v) {
		switch(v.getId()) {
			case R.id.btnMainAdicionarTicket:
				startActivity(new Intent(this, AdicionarTicketActivity.class));
				break;
			case R.id.btnMainListarTickets:
				startActivity(new Intent(this, ListarTicketsActivity.class));
				break;
			case R.id.btnMainBuscarTickets:
				startActivity(new Intent(this, BuscarTicketsActivity.class));
				break;
			case R.id.btnMainExpirarTickets:
				StorageTicketService.getInstance().expire(this);
				break;
			case R.id.btnMainDeletarTicket:
				startActivity(new Intent(this, DeleteTicketActivity.class));
				break;
			case R.id.btnMainRegistrarCelular:
				startActivity(new Intent(this, RegistrarPortalActivity.class));
				break;
			case R.id.btnMainEnviarTickets:
				StorageTicketService storage = StorageTicketService.getInstance();
				storage.search(new Date(), this);
				break;
		}
	}

	@Override
	public void onSave(boolean result) {
	}

	@Override
	public void onSearch(ArrayList<Ticket> tickets) {
		if ( tickets != null ) {
			String msg = "";
			for ( Ticket tmp : tickets )
				msg += tmp.toString() + "@";
			NdefRecord ndefRecord = NdefRecord.createMime("application/vnd.br.com.bandtec.blueticket.android.ui.blueticket", 
					msg.getBytes());
			NfcAdapter.getDefaultAdapter(this).setNdefPushMessage(
					new NdefMessage(ndefRecord), this);
		}
	}

	@Override
	public void onExpire(boolean result) {
		if ( result ) {
			Toast.makeText(this, "Tickets expirados com sucesso", Toast.LENGTH_LONG).show();
		} else {
			TicketToastErrors.toastErrors(this);
		}
	}

	@Override
	public void onDelete(boolean result) {
	}

	@Override
	public void onList(ArrayList<Ticket> tickets) {
	}
}

