from django.conf.urls import patterns, include, url

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'blueticketserver.views.home', name='home'),
    # url(r'^blueticketserver/', include('blueticketserver.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
    url(r'^$', 'ticketsend.views.ticketsend_index', name='ticketsend_index'),
    url(r'^usuario/adicionar$', 'ticketsend.views.usuario_adicionar', name='usuario_adicionar'),
    url(r'^usuario/listar$', 'ticketsend.views.usuario_listar', name='usuario_listar'),
    url(r'^ticket/adicionar$', 'ticketsend.views.ticket_adicionar', name='ticket_adicionar'),
    url(r'^ticket/listar$', 'ticketsend.views.ticket_listar', name='ticket_listar'),
    url(r'^evento/adicionar$', 'ticketsend.views.evento_adicionar', name='evento_adicionar'),
    url(r'^evento/listar$', 'ticketsend.views.evento_listar', name='evento_listar'),
    url(r'^ticket/enviar/(\d+)$', 'ticketsend.views.ticket_enviar', name='ticket_enviar'),
    url(r'^celular/registrar/(\w+)$', 'ticketsend.views.celular_registrar', name='celular_registrar'),
    url(r'^servidor/consultar/(\d+)$', 'ticketsend.views.servidor_consultar', name='servidor_consultar'),
)
