from django.db import models
from django.forms import ModelForm

import hashlib


class Usuario(models.Model):
	usuario = models.CharField(max_length=25, null=False, blank=False)
	cpf = models.CharField(max_length=15, null=False, blank=False, unique=True)
	gcmid = models.CharField(max_length=100, null=True, blank=True)
	
	def __repr__(self):
		return self.usuario
		
	def __unicode__(self):
		return self.__repr__()
	
	
class UsuarioForm(ModelForm):
	class Meta(object):
		model = Usuario



class Evento(models.Model):
	nome = models.CharField(max_length=35, null=False, blank=False)
	
	def __repr__(self):
		return self.nome
		
	def __unicode__(self):
		return self.__repr__()
		
		
class EventoForm(ModelForm):
	class Meta(object):
		model = Evento
		
		
		
class Ticket(models.Model):
	usuario = models.ForeignKey('Usuario')
	data_venda = models.CharField(max_length=10, null=False, blank=False)
	data_vencimento = models.CharField(max_length=10, null=False, blank=False)
	token = models.CharField(max_length=35, null=True, blank=True)
	evento = models.ForeignKey('Evento')
	
	def __repr__(self):
		return self.id
		
	def __unicode__(self):
		return self.__repr__()
		
	def gen_token(self):
		tmp = self.data_venda
		tmp += '|' + self.data_vencimento
		tmp += '|' + str(self.id)
		tmp += '|' + self.usuario.usuario
		tmp += '|' + self.usuario.cpf
		
		md5 = hashlib.md5()
		md5.update(tmp)
		self.token = md5.hexdigest()
		self.save()
		
		tmp += '|' + self.token + '|'
		return tmp
	
	
class TicketForm(ModelForm):
	class Meta(object):
		model = Ticket
