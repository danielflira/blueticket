from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render_to_response
from ticketsend.models import Usuario, UsuarioForm, Ticket, TicketForm, Evento, EventoForm
from django.core.context_processors import csrf

import urllib2
import json
from collections import OrderedDict



def get_fields(model):
	fields = []
	for i in model._meta.fields:
		fields.append(i.name)
	return fields
	
	
	
def get_list_values(objects):
	if objects == None or len(objects) == 0:
		return []
	fields_names = get_fields(objects[0])
	fields_values = []
	for i in objects:
		tmp = OrderedDict()
		for j in fields_names:
			tmp[j] = (getattr(i, j))
		fields_values.append(tmp)
	return fields_values
	
	
	
def send_gcm(gcm_id, gcm_targets, gcm_message):
	gcm_data = json.dumps({
		'registration_ids': gcm_targets,
		'data' : {'message': gcm_message},
	})
	gcm_headers = {
		'Authorization': 'key=' + gcm_id,
		'Content-Type': 'application/json',
	}
	gcm = urllib2.Request("https://android.googleapis.com/gcm/send", gcm_data, gcm_headers)
	f = urllib2.urlopen(gcm)
	
	return f.read() + gcm_message



def usuario_adicionar(request):
	if request.method == 'POST':
		form = UsuarioForm(request.POST)
		if form.is_valid():
			form.save()
			return HttpResponseRedirect(reverse('usuario_listar'))
	else:
		form = UsuarioForm()
	c = {'form': form}
	c.update(csrf(request))
	return render_to_response('ticketsend/adicionar.html', c)
	
	
	
def usuario_listar(request):
	c = {}
	c.update({'data': get_list_values(Usuario.objects.all())})
	return render_to_response('ticketsend/listar.html', c)
	
	
	
def ticket_adicionar(request):
	if request.method == 'POST':
		form = TicketForm(request.POST)
		if form.is_valid():
			form.save()
			return HttpResponseRedirect(reverse('ticket_listar'))
	else:
		form = TicketForm()
	c = {'form': form}
	c.update(csrf(request))
	return render_to_response('ticketsend/adicionar.html', c)
	
	
	
def ticket_listar(request):
	c = {}
	c.update({'data': get_list_values(Ticket.objects.all())})
	return render_to_response('ticketsend/listar_ticket.html', c)
	
	
	
def ticket_enviar(request, ticket_id):
	ticket = Ticket.objects.get(id=ticket_id)
	gcm_id = 'AIzaSyAa3jys1niTAFQru05OUqTJHMMiIo_RGiM'
	gcm_targets = [ticket.usuario.gcmid]
	gcm_message = ticket.gen_token()
	return HttpResponse(send_gcm(gcm_id, gcm_targets, gcm_message))



def celular_registrar(request, cpf):
	try:
		usuario = Usuario.objects.get(cpf=cpf)
		usuario.gcmid = request.POST["gcmid"]
		usuario.save()
		return HttpResponse('Ok')
	except:
		return HttpResponse('Error 1: Usuario nao existe')
		
	
	
def servidor_consultar(request, evento_id):
	try:
		tmp = ''
		evento = Evento.objects.get(id=evento_id)
		for i in evento.ticket_set.all():
			tmp += i.gen_token()[:-1] + '@'
		return HttpResponse(tmp)
	except:
		return HttpResponse('Error 1: Evento nao existe')
	


def ticketsend_index(request):
	return render_to_response('ticketsend/base.html')
	
	
	
def evento_adicionar(request):
	if request.method == 'POST':
		form = EventoForm(request.POST)
		if form.is_valid():
			form.save()
			return HttpResponseRedirect(reverse('evento_listar'))
	else:
		form = EventoForm()
	c = {'form': form}
	c.update(csrf(request))
	return render_to_response('ticketsend/adicionar.html', c)
	
	
	
def evento_listar(request):
	c = {}
	c.update({'data': get_list_values(Evento.objects.all())})
	return render_to_response('ticketsend/listar.html', c)
